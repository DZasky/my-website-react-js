import React from 'react';
import './LandingPage.css';

function LandingPage () {
  return (
    <section className="landing-page" id="landing-page">
            <div className="landing-page-shadow">
                <div className="container">
                    <div className="landing-page-text">
                    <h1>Nasza firma oferuje najwyższej jakości usługi.</h1>
                    <h2>Nie wierz nam na słowo - sprawdź</h2>
                    <button className="landing-page-btn">oferta</button>
                    </div>
                </div>
            </div>
    </section>
  );
}

export default LandingPage;
