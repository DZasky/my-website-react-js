import React from 'react';
import './Offer.css';

function Offer () {
  return (
<section className="offer" id="offer">
    <div className="container">
        <h1>Czym się zajmuje nasza firma?</h1>
            <div className="offer-box-wrapper">
                <div className="offer-box">
                    <div className="offer-box-content-circle"></div>
                    <div className="offer-box-content">
                        Usługa 1
                        <div className="offer-box-content-new">(nowość)</div>
                    </div>
                </div>
                
                <div className="offer-box">
                    <div className="offer-box-content">
                        Usługa 2
                    </div>
                </div>
                <div className="offer-box">
                    <div className="offer-box-content">
                        Usługa 3
                    </div>
                </div>
                <div className="offer-box">
                    <div className="offer-box-content">
                        Usługa 4
                    </div>
                </div>
                <div className="offer-box">
                    <div className="offer-box-content">
                        Usługa 5
                    </div>
                </div>
                <div className="offer-box">
                    <div className="offer-box-content">
                        Usługa 6
                    </div>
                </div>
            </div>
    </div>
</section>
  );
}

export default Offer;
