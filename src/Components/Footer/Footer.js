import React from 'react';
import './Footer.css';

function Footer () {
  return (
<footer>
    <div className='container'>
        <div className="footer-container-end-credits">
            Damian Zajączkowski - wszelkie prawa zastrzeżone, 2019
        </div>
        <div className="footer-container-social-ref">
            <a href="https://www.instagram.com/"><i className="fa fa-instagram footer-icon"></i></a>
            <a href="https://www.facebook.com/"><i className="fa fa-facebook-square footer-icon"></i></a>
        </div>
    </div>
</footer>
  );
}

export default Footer;
