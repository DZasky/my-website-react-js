import React from 'react';
import './About.css';

function About () {
  return (
<section className="about" id="about">
     <div className="container">
        <h1>Nasz Zespół</h1>
                <div className="about-employee">
                    <div className="about-employee-img1"></div>
                        <div className="about-employee-text">
                            <h2>Jerzy [Project Manager]</h2>
                            <p>
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. 
                                Vitae pariatur est labore libero? Eius quo mollitia repellendus 
                                omnis tenetur error, magni nostrum distinctio possimus, totam 
                                doloribus minus, ipsam corrupti fuga.
                            </p>
                        </div>
                </div>
                <div className="about-employee">
                    <div className="about-employee-img2"></div>
                        <div className="about-employee-text">
                            <h2>Tomasz [Robotnik]</h2>
                            <p>
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. 
                            Vitae pariatur est labore libero? Eius quo mollitia repellendus 
                            omnis tenetur error, magni nostrum distinctio possimus, totam 
                            doloribus minus, ipsam corrupti fuga.
                            </p>
                        </div>
                </div>
    </div>
</section>
  );
}

export default About;
