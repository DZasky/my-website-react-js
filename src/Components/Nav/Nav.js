import React from 'react';
import './Nav.css';

function Nav() {
  return (
    <nav>
    <div className="container">
      <a href="#landing-page" className="my-name">Damian Zajączkowski</a>
      <div className="nav-links">
       <a href="#about">O mnie</a>
       <a href="#offer">Usługi</a>
       <a className="kontakt" href="#">Kontakt</a>
       </div>
    </div>
    </nav>
  );
}

export default Nav;
